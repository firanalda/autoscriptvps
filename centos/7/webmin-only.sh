#!/bin/bash

ip=`wget -q -O - http://ip.keithscode.com`

wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -i epel-release-latest-7.noarch.rpm

echo '[Webmin]' > /etc/yum.repos.d/webmin.repo
echo 'name=Webmin Distribution Neutral' >> /etc/yum.repos.d/webmin.repo
echo '#baseurl=http://download.webmin.com/download/yum' >> /etc/yum.repos.d/webmin.repo
echo 'mirrorlist=http://download.webmin.com/download/yum/mirrorlist' >> /etc/yum.repos.d/webmin.repo
echo 'enabled=1' >> /etc/yum.repos.d/webmin.repo

cd /tmp && wget http://www.webmin.com/jcameron-key.asc
rpm --import http://www.webmin.com/jcameron-key.asc

yum install webmin -y
systemctl enable webmin
systemctl start webmin

reset

echo "################################################"
echo "Webmin Panel: https://$ip:10000"
echo "################################################"
echo
echo